import React, { useState } from 'react'
import Round from './Round'

export default function Player() {
  const startingCards = 5
  const rounds = []

  const [name, setName] = useState('Player')

  let index = 1
  for (let cards = startingCards; cards >= -startingCards; cards = cards - 1) {
    if (cards === 0) continue
    rounds.push(<Round cards={cards} key={cards} />)
    index = index + 1
  }

  return (
    <>
      <input className='name' value={name} onChange={({target}) => setName(target.value)}/>
      {rounds}
    </>
  )
}
