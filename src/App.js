import React, { useState, useEffect } from 'react'
import Player from './Player'

function App() {
  const [ players, setPlayers ] = useState(3)
  const cols = []

  useEffect(() => {
    document.documentElement.style.setProperty('--players', players)
  }, [players])

  for(let i = 0; i < players; i = i + 1) {
    cols.push(<Player key={i}/>)
  }

  return (
    <div className="App">
      <button onClick={() => setPlayers(players - 1)}>-</button>
      {players}
      <button onClick={() => setPlayers(players + 1)}>+</button>
      <div className="score-board">
        {cols}
      </div>
    </div>
  )
}

export default App
