import React, { useState, useEffect } from 'react'

export default function Round({cards}) {
  const [prediction, setPrediction] = useState()
  const [score, setScore] = useState()
  const [tricks, setTricks] = useState()

  useEffect(() => {
    console.log(prediction, tricks)
    const points = tricks * 2
    const bonusPrediction = tricks === prediction ? 10 : 0
    const bonusPerfect = tricks === cards ? 5 : 0
    setScore(points + bonusPrediction + bonusPerfect || '')
  }, [prediction, tricks, cards])

  return (
    <div className='round'>
      <input
        className='prediction'
        type='number'
        value={prediction}
        onChange={e => setPrediction(Number(e.target.value))}
      />
      <input className='score' type='number' value={score} disabled/>
      <input
        className='tricks'
        type='number'
        value={tricks}
        onChange={e => setTricks(Number(e.target.value))}
      />
    </div>
  )
}
